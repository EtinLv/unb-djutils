"""
Extra permissions for use in Django REST Framework views.

"""

from rest_framework import permissions


class IsStaff(permissions.BasePermission):
  """Only allow users where user.is_staff is True."""

  def has_permission(self, request, view):
    return request.user.is_staff


class IsAdmin(permissions.BasePermission):
  """Only allow users where user.is_superuser is True."""

  def has_permission(self, request, view):
    return request.user.is_superuser
