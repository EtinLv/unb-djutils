"""
A package of modules for building REST APIs.

Many modules here assume that
[Django REST Framework](http://www.django-rest-framework.org/)
is being used, and is available to import from the environment as
`rest_framework`.

"""
