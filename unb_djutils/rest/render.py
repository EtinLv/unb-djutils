"""
Common renderers.

"""

from rest_framework import renderers


def to_json(request, qs, serializer, many=False):
  s = serializer(qs, context={'request': request}, many=many)
  return renderers.JSONRenderer().render(s.data)
