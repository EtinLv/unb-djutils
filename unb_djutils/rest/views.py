"""
Resource Views
==============

Generic resource views and shortcuts for common actions.

"""

from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status


def save_and_return(r):
  """Try to save a resource, return success/failure responses appropriately."""
  try:
    r.save()
    return Response(r.data)
  except r.ValidationError:
    return Response(r.errors, status=status.HTTP_400_BAD_REQUEST)


# Base Resource View
# ==================

class BaseResourceView(APIView):
  def valid(self, resource):
    return Response(resource.data)

  def invalid(self, resource):
    return Response(resource.errors, status=status.HTTP_400_BAD_REQUEST)


# Mixins
# ======

class CreateMixin(object):
  def create(self, request, *args, **kwargs):
    r = self.resource(request, *args, **kwargs)
    try:
      r.create()
      return self.valid(r)
    except r.ValidationError:
      return self.invalid(r)


class ReadMixin(object):
  def read(self, request, *args, **kwargs):
    r = self.resource(request, *args, **kwargs)
    try:
      r.read()
      return self.valid(r)
    except r.ValidationError:
      return self.invalid(r)


class UpdateMixin(object):
  def update(self, request, *args, **kwargs):
    r = self.resource(request, *args, **kwargs)
    try:
      r.update()
      return self.valid(r)
    except r.ValidationError:
      return self.invalid(r)


class DeleteMixin(object):
  def destroy(self, request, *args, **kwargs):
    r = self.resource(request, *args, **kwargs)
    try:
      r.delete()
      return Response(status=status.HTTP_204_NO_CONTENT)
    except r.ValidationError:
      return self.invalid(r)


class ListMixin(object):
  def list(self, request, *args, **kwargs):
    r = self.resource(request, *args, **kwargs)
    try:
      r.list()
      return self.valid(r)
    except r.ValidationError:
      return self.invalid(r)


# Resource Views
# ==============

class ResourceListCreateView(ListMixin, CreateMixin, BaseResourceView):
  def get(self, request, *args, **kwargs):
    return self.list(request, *args, **kwargs)

  def post(self, request, *args, **kwargs):
    return self.create(request, *args, **kwargs)


class ResourceListView(ListMixin, BaseResourceView):
  def get(self, request, *args, **kwargs):
    return self.list(request, *args, **kwargs)


class ResourceCreateView(CreateMixin, BaseResourceView):
  def post(self, request, *args, **kwargs):
    return self.create(request, *args, **kwargs)


class ResourceReadUpdateDeleteView(ReadMixin, UpdateMixin, DeleteMixin,
                                   BaseResourceView):
  def get(self, request, *args, **kwargs):
    return self.read(request, *args, **kwargs)

  def put(self, request, *args, **kwargs):
    return self.update(request, *args, **kwargs)

  def delete(self, request, *args, **kwargs):
    return self.destroy(request, *args, **kwargs)


class ResourceReadUpdateView(ReadMixin, UpdateMixin, BaseResourceView):
  def get(self, request, *args, **kwargs):
    return self.read(request, *args, **kwargs)

  def put(self, request, *args, **kwargs):
    return self.update(request, *args, **kwargs)


class ResourceReadDeleteView(ReadMixin, DeleteMixin, BaseResourceView):
  def get(self, request, *args, **kwargs):
    return self.read(request, *args, **kwargs)

  def delete(self, request, *args, **kwargs):
    return self.destroy(request, *args, **kwargs)


class ResourceUpdateDeleteView(UpdateMixin, DeleteMixin, BaseResourceView):
  def put(self, request, *args, **kwargs):
    return self.update(request, *args, **kwargs)

  def delete(self, request, *args, **kwargs):
    return self.destroy(request, *args, **kwargs)


class ResourceReadView(ReadMixin, BaseResourceView):
  def get(self, request, *args, **kwargs):
    return self.read(request, *args, **kwargs)


class ResourceUpdateView(UpdateMixin, BaseResourceView):
  def put(self, request, *args, **kwargs):
    return self.update(request, *args, **kwargs)


class ResourceDeleteView(DeleteMixin, BaseResourceView):
  def delete(self, request, *args, **kwargs):
    return self.destroy(request, *args, **kwargs)
