from django import shortcuts as s
from django.contrib.auth import decorators
from django.utils.decorators import method_decorator


# Function Based View Decorators
# ==============================

def redirect_authenticated(redirect_url):
  """Redirect authenticated users.

  Args:
    redirect_url: Any pattern that django.shortcuts.resolve_url can resolve.

  Example:
    Showing a login page to a user that is already authenticated doesn't make
    much sense.  Use ``redirect_authenticated`` to have them skip it.

      @redirect_authenticated('home')
      def login(request):
        pass
  """
  def decorator(func):
    def wrapper(request, *args, **kwargs):
      if request.user.is_authenticated():
        return s.redirect(redirect_url)
      else:
        return func(request, *args, **kwargs)
    return wrapper
  return decorator


# Class Based View Decorators
# ===========================

def add_decoration(function_decorator):
  """Convert function decorator to method decorator and decorate dispatch."""
  def class_based_decorator(View):
    """Takes a class-based view and decorates the dispatch method."""
    View.dispatch = method_decorator(function_decorator)(View.dispatch)
    return View
  return class_based_decorator


login_required = add_decoration(decorators.login_required)
