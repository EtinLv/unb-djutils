"""
Reform Utils
============

Utilities useful when working with Reform.js.

"""


# TODO(nick): This is probably a bad idea.  Not only is it location specific,
#     it also doesn't support extensions, or different formatting (e.g.,
#     555-555-5555 vs (555) 555-5555 vs (555)555-5555).
def phone_mask(val):
  """Format a US phone number string based on length.

  For `val < 7` format the number as xxx-xxxx
  For `7 < val < 10` format the number as `(xxx) xxx-xxxx`

  Args
  ----
  val : string
      A string of numbers (0-9) to be formatted as a phone number.

  Returns
  -------
  string
      A string containing the numbers in val interspersed with format
      characters.

  """
  if not val:
    return val

  # Format: `xxx-xxxx`
  if 3 < len(val) <= 7:
    return val[0:3] + '-' + val[3:]

  # Format: `(xxx) xxx-xxxx`  also truncates numbers over 10 digits.
  if len(val) > 7:
    return ('(' + val[:3] + ') ' +
            val[3:6] + '-' +
            val[6:min(len(val), 10)])

  # Format: `xxx`
  return val
