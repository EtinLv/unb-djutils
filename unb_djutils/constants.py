"""
Constants
#########

Commonly used constants.

"""


class USTimeZone(object):
  EST = 'EST'
  CST = 'CST'
  MST = 'MST'
  PST = 'PST'
