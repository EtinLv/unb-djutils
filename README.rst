UNB Django Utils
================

A package of general Django utilities.


Documentation
=============

Documentation for UNB Django Utils can be found in the ``/docs`` directory.


Issue Reporting and Contact Information
=======================================

If you have any problems with this software, please take a moment to report
them at https://bitbucket.org/unbsolutions/unb-djutils/issues or  by email to
nick@unb.services.

If you are a security researcher or believe you have found a security
vulnerability in this software, please contact us by email at
nick@unb.services.


Copyright and License Information
=================================

Copyright (c) 2015 Nick Zarczynski

This project is licensed under the MIT license.  Please see the LICENSE file
for more information.
