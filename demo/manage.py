#!/usr/bin/env python
import os
import sys

if __name__ == "__main__":
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "demo.settings")

    DEMO_DIR = os.path.dirname(os.path.abspath(__file__))
    PROJECT_DIR = os.path.dirname(DEMO_DIR)

    if PROJECT_DIR not in sys.path:
        sys.path.insert(0, PROJECT_DIR)

    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
